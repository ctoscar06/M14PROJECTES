function initdb(){
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
	slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
	chown -R openldap.openldap /etc/ldap/slapd.d
	chown -R openldap.openldap /var/lib/ldap
	/usr/sbin/slapd -d0
}

function slapd(){
	rm -rf /etc/ldap/slapd.d/*
	rm -rf /var/lib/ldap/*
	slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
	chown -R openldap.openldap /etc/ldap/slapd.d
	chown -R openldap.openldap /var/lib/ldap
	/usr/sbin/slapd -d0
}

function edt(){
	/usr/sbin/slapd -d0

}

case $1 in
	"initdb")
		echo "initdb" && initdb
		;;
	"slapd")
		echo "slapd" && slapd
		;;
	"start")
		echo "start" && edt
		;;
	"slapcat")
		if [ -n $2 ]; then
			slapcat -n $2
		else
			echo "Especifica la DB"
		fi
		;;
esac


