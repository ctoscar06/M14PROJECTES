-- Crear una base de datos
CREATE DATABASE mydb;

-- Usar la base de datos recién creada
\c mydb;




CREATE TABLE Futbolistas (
    id SERIAL PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL,
    equipo VARCHAR(100),
    edad INT,
    posicion VARCHAR(50),
    nacionalidad VARCHAR(50)
);

INSERT INTO Futbolistas (nombre, equipo, edad, posicion, nacionalidad) VALUES
    ('Lionel Messi', 'Paris Saint-Germain', 34, 'Delantero', 'Argentino'),
    ('Cristiano Ronaldo', 'Manchester United', 36, 'Delantero', 'Portugués'),
    ('Neymar Jr.', 'Paris Saint-Germain', 29, 'Delantero', 'Brasileño'),
    ('Kylian Mbappé', 'Paris Saint-Germain', 22, 'Delantero', 'Francés'),
    ('Kevin De Bruyne', 'Manchester City', 30, 'Centrocampista', 'Belga');
